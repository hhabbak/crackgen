#!/usr/bin/env python3

import os
import getopt
import sys
import zipfile
import random
import lxml.html
import requests

url = "https://crackmes.one/"
url_search = url + "search"
passwords = ["crackmes.one", "crackmes.de"]
difficulty = [
        "Very Easy",
        "Easy",
        "Medium",
        "Hard",
        "Very Hard",
        "Insane"
]
platform = [
        "Dos",
        "Max Os X",
        "Multiplatform",
        "Unix/Linux etc.",
        "Windows",
        "Windows 2000/XP Only",
        "Windows 7 Only",
        "Windows Vista Only",
        "Unspecified/other"
]
langage = [
        "C/C++",
        "Assembler",
        "Java",
        "(Visual) Basic",
        "Borland Delphi",
        "Turbo Pascal",
        ".NET",
        "Unspecified/other"
]

querry = {
        "difficulty-min" : "1",
        "difficulty-max" : "6",
        "quality-min" : "1",
        "quality-max" : "6",
}

def usage():
    print("{0} -h -n [name] -q [1-6] -d [difficulty] -l [langage] -p [platform]".format(sys.argv[0]))
    dif = ["{0} {1}".format(i, d) for i, d in enumerate(difficulty, 1)]
    print("Quality: 1-6")
    print("Difficulty:\n {0}".format("\n ".join(dif)))
    print("Langage:\n {0}".format("\n ".join(langage)))
    print("Platform:\n {0}".format("\n ".join(platform)))
    sys.exit(2)

arg = sys.argv[1:]
try:
    opts, args = getopt.getopt(arg,"hn:d:l:p:q:",["name=", "difficulty=", "langage=", "platform=", "quality="])
except getopt.GetoptError as err:
    print(err)
    usage()

for opt, arg in opts:
    if (opt == "-h"):
        usage()
    if (opt == "-n"):
        querry["name"] = arg
    elif (opt == "-d"):
        if arg.isdigit() and int(arg) > 0 and int(arg) < len(difficulty):
            querry["difficulty-min"] = arg
            querry["difficulty-max"] = arg
        else:
            arg = arg.lower()
            arg = [i for i, d in enumerate(difficulty, 1) if d.lower().startswith(arg)]
            if arg:
                querry["difficulty-min"] = arg[0]
                querry["difficulty-max"] = arg[0]
    elif (opt == "-l"):
        arg = arg.lower()
        arg = [l for l in langage if arg in l.lower()]
        if arg:
            querry["lang"] = arg[0]
    elif (opt == "-p"):
        arg = arg.lower()
        arg = [p for p in platform if arg in p.lower()]
        if arg:
            querry["platform"] = arg[0]
    elif (opt == "-q"):
        querry["quality-min"] = arg
        querry["quality-max"] = arg

s = requests.session()
req = s.get(url_search)
for chunk in req.iter_content(chunk_size = 256):
    chunk = chunk.decode("utf-8")
    if "token" in chunk:
        idx = chunk.find("value=") + 7
        querry["token"] = chunk[idx:idx + 32]

req = s.post(url_search, data=querry, cookies=req.cookies)

crackme = {}
root = lxml.html.fromstring(req.text)
rows = root.xpath("//div/table/tbody/tr")

if not rows:
    print("no crack.")
    sys.exit(0)

while len(rows) > 0:
    row = random.choice(rows)
    rows.remove(row)

    meta = row.xpath("./td/text()")[3::]
    crackme["langage"] = meta[0].strip()
    crackme["arch"] = meta[1].strip()
    crackme["difficulty"] = meta[2].strip()
    crackme["quality"] = meta[3].strip()
    crackme["platform"] = meta[4].strip()

    meta = row.xpath("./td/a/text()")
    crackme["name"] = meta[0]
    crackme["author"] = meta[1]
    meta = row.xpath("./td/a/@href")
    crackme["url"] = meta[0]

    req = s.get(url + crackme["url"])
    root = lxml.html.fromstring(req.text)

    download = root.xpath("//div/div/div/a/@href")[0]
    crackme["download"] = download
    desc = root.xpath("//div/div/div/p/span/text()")[0]
    crackme["desc"] = desc

    req = s.get(url + crackme["download"])
    zip_name = crackme["name"] + ".zip"
    path_extract = crackme["name"] + "/"
    if os.path.exists(path_extract):
        continue
    os.mkdir(path_extract)
    with open(zip_name, "wb") as f:
        for chunk in req:
            f.write(chunk)
        f.close()
        for p in passwords:
            zip_ref = zipfile.ZipFile(zip_name, "r")
            try:
                zip_ref.extractall(path_extract, pwd=str.encode(p))
                crackme["files"] = [ path_extract + f for f in zip_ref.namelist()]
                break
            except:
                pass
        os.remove(zip_name)

    print("Name:\n {0} - {1}".format(crackme["name"], crackme["author"]))
    print("Description:\n {0}".format(crackme["desc"]))
    print("Arch:\n {0}".format(crackme["arch"]))
    print("Difficulty:\n {0}".format(crackme["difficulty"]))
    print("Quality:\n {0}".format(crackme["quality"]))
    print("Langage:\n {0}".format(crackme["langage"]))
    print("Platform:\n {0}".format(crackme["platform"]))
    print("Extract:\n {0}".format("\n".join(crackme["files"])))
    exit(0)

print("no crack.")
